import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns

from pyLE import utils, env, chemistry, sources_contributions


"""
Module use to generate plots for article
"""


def plot_common_date_pmf_le(stations=None, pmfs=None, savefig=False):
    """Plot common date between PMF and LE

    Parameters
    ----------

    stations : list, default: env.STATIONS
        If None, retrieve all STATIONS.
    pmfs : list of PMF, default None
    savefig: boolean, default: False
    """

    if stations is None:
        stations = env.STATIONS

    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    else:
        pmfs = {key: val for key, val in pmfs.items() if key in stations}

    dfPMF = pd.concat({k: pmf.dfcontrib_c for k, pmf in pmfs.items()})
    dfPMF = dfPMF.reset_index().set_index(["level_0", "Date"])
    dfPMF.index.names = ["Station", "Date"]

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 4))

    sites_order = [x for x in env.SITES_ORDER if x in stations][::-1]
    for i, station in enumerate(sites_order):
        facecolor = utils.get_typology_color(station)
        edgecolor = "white"  # sns.desaturate(facecolor)
        plot_ops = dict(
            alpha=1.0,
            color=facecolor,
            label=station,
            markerfacecolor=edgecolor,
            markersize=10,
            markeredgewidth=0.5,
        )
        date = dfPMF.loc[station].index
        ax.plot(date, [i] * len(date), "-o", **plot_ops)

    datemin = pd.to_datetime("2013-01-01")
    datemax = pd.to_datetime("2017-01-01")
    start = mdates.date2num(datemin)
    end = mdates.date2num(datemax)
    width = end - start
    rec = plt.Rectangle(
        xy=(start, -0.5),
        width=width,
        height=len(stations) + 0.5,
        color="lightgrey",
        alpha=0.5,
        zorder=-1,
    )
    ax.add_patch(rec)

    ntypos = utils.get_count_by_typology(stations)
    for k, v in ntypos.items():
        if k == "Remote":
            y = len(stations)
        elif k == "Urban":
            y -= ntypos["Remote"]
        elif k == "Urban valley":
            y -= ntypos["Urban"]
        elif k == "Industrial":
            y -= ntypos["Urban valley"]
        elif k == "Traffic":
            y -= ntypos["Industrial"]

        ax.annotate(
            k, (1.0, (y - 0.5) / len(stations)), xycoords="axes fraction", va="center"
        )
        ax.plot(
            [end + 50, end + 50],
            [y - 0.7, y - ntypos[k] - 0.3],
            "-",
            color=env.TYPOLOGIES[k]["color"],
            lw=4,
        )

    ax.set_yticks(range(len(stations)))
    ax.set_ylim(-0.5, len(stations) - 0.5)
    ax.set_yticklabels(sites_order)
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y"))
    sns.despine()
    fig.subplots_adjust(right=0.83)

    if savefig:
        title = env.DIR_FIGURES + "/Common_date/common_date_PMF_LE"
        plt.savefig(title + ".png")
        plt.savefig(title + ".pdf")


def plot_chemistry_obs_vs_le(stations, species, savefig):
    """Comparison between observation and LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    savefig : boolean

    """
    chemistry.compare_pmf_le_all(stations=stations, species=species, savefig=savefig)

    df_fit = chemistry.aggregate_comparison_all_stations_species(
        stations=stations, species=species, savefig=savefig
    )

    table = chemistry.fit_to_table_per_specie(df_fit)
    if savefig:
        table.to_csv(env.DIR_FIGURES + "/correlation_LOTOS-PMF.csv")
    else:
        print(table)


def plot_all(savefig=True):
    """Plot all figures for articles and more

    Parameters
    ----------

    savefig : boolean, default: True
        Save the figures as png and pdf
    """

    stations = env.STATIONS
    species = ["PM10", "OC", "EC", "NO3-", "SO42-", "Na+"]
    pmfs = utils.get_pmfs(stations)

    plt.interactive(False)
    # ==== Methods : common date between PMF and LE
    plot_common_date_pmf_le(pmfs=pmfs, savefig=savefig)

    # ==== Comparison Chemistry / LE
    plot_chemistry_obs_vs_le(stations=stations, species=species, savefig=savefig)

    # ==== Compare specific sources
    sources_contributions.plot_all_sources_comparison(
        stations=stations, pmfs=pmfs, savefig=savefig
    )
    # ====
    plt.interactive(True)
