from os import path
import collections

BASE_DIR = "/home/webersa/Documents/LOTOS-EUROS/comparison-pmf-le/"
DIR_LE_DATA = path.normpath(BASE_DIR + "/data/")
DIR_LE_PER_STATION = path.normpath(BASE_DIR + "/data_per_station/")
DIR_FIGURES = path.normpath(BASE_DIR + "/figures/")

LE_NETCDF_FILENAME = "LE_PM-SoAp-FRA-FRA-{year}_labelled-conc-sfc_{date}.nc"

DB_PMF = path.normpath(DIR_LE_PER_STATION + "/DB_PMF.db")

STATIONS = [
    "STG-cle",
    "RBX",
    "LEN",
    "TAL",
    # "LYN",
    "GRE-fr",
    "CHAM",
    "REV",
    "MRS-5av",
    "NIC",
    "NGT",
    "ROU",
    "AIX",
    "PdB",
    # "POI",
]
STATIONS_OP = [
    "Talence",
    "STG-cle",
    "Rouen",
    "Roubaix",
    "PdB",
    "Passy",
    "Nogent",
    "Nice",
    "MRS-5av",
    "GRE-fr",
    "Chamonix",
    "Aix-en-provence",
]

STATIONS_SHORT2LONG = {
    "CHAM": "Chamonix",
    "RBX": "Roubaix",
    "LYN": "Lyon",
    "REV": "Revin",
    "NIC": "Nice",
    "NGT": "Nogent",
    "PAS": "Passy",
    "TAL": "Talence",
    "ROU": "Rouen",
    "AIX": "Aix-en-provence",
    "LEN": "Lens",
}
STATIONS_LONG2SHORT = {v: k for k, v in STATIONS_SHORT2LONG.items()}

LE_VARIABLES = [
    "no",
    "no2",
    "nh3",
    "so2",
    "hno3",
    "tpm10",
    "tpm25",
    "ec_f",
    "ec_c",
    "pom_f",
    "pom_c",
    "ppm_f",
    "ppm_c",
    "no3a_f",
    "no3a_c",
    "nh4a_f",
    "so4a_f",
    "so4a_c",
    "tpm25_comb",
    "tpm10_comb",
    "tss",
    "tdust",
]

LE_LABELS = {
    "Coal": [0, 3, 11, 14],
    "Biomass": [1, 4, 12, 15],
    "Other": [2, 5, 10, 13, 16, 21],
    "RoadTrans": [6, 17],
    "NonRoadTrans": [7, 18],
    "Industry": [8, 19],
    "Agriculture": [9, 20],
    "Natural": [22],
    "Boundary": [23],
}

TYPOLOGIES = collections.OrderedDict(
    [
        ("Remote", dict(site=["REV"], color="#d6f5d6", marker="*")),
        (
            "Urban",
            dict(
                site=[
                    "MRS-5av",
                    "NIC",
                    "AIX",
                    "TAL",
                    "POI",
                    "LEN",
                    "NGT",
                    "ROU",
                    "LYN",
                ],
                color="#efcac2",
                marker="o",
            ),
        ),
        ("Urban valley", dict(site=["GRE-fr", "CHAM"], color="#f9f0e7", marker="^")),
        ("Industrial", dict(site=["PdB"], color="#c490d0", marker="P")),
        ("Traffic", dict(site=["RBX", "STG-cle"], color="#bbbed3", marker="p")),
    ]
)

SITES_ORDER = [site for x in TYPOLOGIES.values() for site in x["site"]]
