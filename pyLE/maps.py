import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import seaborn as sns
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.io.img_tiles as cimgt
from adjustText import adjust_text

from . import utils
from .env import LE_VARIABLES, LE_NETCDF_FILENAME, TYPOLOGIES, DIR_FIGURES


def plot_animated_map(start, end, variable="tpm10", label="all"):
    """Create a matplotlib annimation for the given variable and label from LE

    Parameters
    ----------
    start : str, %Y-%m-%d
        Starting date of the animation
    end : str, %Y-%m-%d
        Ending date of the animation
    variable : str, tpm10
        Variable to plot
    label : str, all
        Label to use for the plot

    Returns
    -------
    ani : matplotlib annimation
    """
    date_range = pd.date_range(start=start, end=end, freq="H")
    excluded_vars = set(VARIABLES) - set([variable])

    vmax = 40

    def make_figure():
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
        ax.coastlines()
        ax.add_feature(cfeature.BORDERS)
        return fig, ax

    def init():
        # ax = plt.axes(projection=ccrs.PlateCarree())
        # ax.coastlines()
        global img
        ds = _get_nc_file(date_range[0], excluded_vars)
        pm = ds[variable].sel(time=date_range[0]).sel(level=0)
        if label == "all":
            pm = pm.sum("label") * 10 ** 9
        else:
            pm = pm.sel(label=LABELS[label]).sum("label") * 10 ** 9

        img = pm.plot(cmap="Oranges", add_colorbar=True, vmin=0, vmax=vmax)
        ax.set_title("{}\n{}".format(label, date_range[0]))
        return img

    def animate(date):
        ds = _get_nc_file(date, excluded_vars)
        pm = ds[variable].sel(time=date).sel(level=0)
        if label == "all":
            pm = pm.sum("label") * 10 ** 9
        else:
            pm = pm.sel(label=LABELS[label]).sum("label") * 10 ** 9

        # remove old plot
        for c in ax.collections:
            c.remove()

        img = pm.plot(cmap="Oranges", add_colorbar=False, vmin=0, vmax=vmax)
        ax.set_title("{}\n{}".format(label, date))
        return img

    fig, ax = make_figure()

    ani = animation.FuncAnimation(
        fig, animate, date_range, blit=False, interval=10, init_func=init, repeat=False
    )
    return ani


def plot_station_on_map(
    stations, background="OSM", zoom_on_stations=False, savefig=False
):
    """Plot the map of the stations

    Parameters
    ----------

    stations : list of str
        Stations to plot
    background : str, {"OSM", "LE"}, default: False
        Background of the map:

          - OSM : openstreetmap
          - LE : LE grid with PM concentration

    zoom_on_stations : boolean, default: False
        Plot also a zoomed map on each station
    savefig : boolean, False
        Either or not save the figure as pnf
    """

    def plot_stations(df, ax, alphapm=1):
        # Station
        for i, lon in enumerate(df["longitude"]):
            if lon > 180:
                lon = lon - 360
                df.loc[i, "longitude"] = lon

        for k, t in TYPOLOGIES.items():
            idx = df.abbrv.isin(t["site"])
            if len(idx) == 0:
                continue
            ax.plot(
                df.loc[idx, "longitude"],
                df.loc[idx, "latitude"],
                label="{}".format(k),
                marker="H",
                markersize=10,  # markeredgecolor="white",
                color=sns.set_hls_values(t["color"], l=0.5, s=1),
                linestyle="",
            )

        texts = []
        for i, t in enumerate(df.abbrv):
            texts.append(
                plt.text(
                    df.loc[i, "longitude"],
                    df.loc[i, "latitude"],
                    t,
                    color="#555",
                    va="center",
                    ha="center"
                    # bbox=dict(facecolor='white', alpha=0.5)
                )
            )
        i = adjust_text(
            texts,
            expand_points=(1.5, 1.5),
            expand_texts=(1.5, 1.5),
            # arrowprops=dict(arrowstyle="->", color='w', lw=0.5)
        )

        ax.legend(frameon=True)
        # plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)
        # plt.tight_layout()

    df = utils.get_station_coordinates(stations)

    fig = plt.figure(figsize=(10, 8))
    ax = plt.subplot(projection=ccrs.PlateCarree())
    ax.cla()
    ax.set_extent([-10, 14, 39, 55])

    # ax.set_extent([5.4, 6.25, 45.0, 45.5]) # GRE-fr zoom
    # ax.set_extent([2.6, 3.45, 50.17, 50.80]) # LENS zoom

    # Background
    if background == "LE":
        excluded_vars = set(LE_VARIABLES) - set(["tpm10"])
        ds = utils._get_nc_file(pd.to_datetime("2014-05-01"), excluded_vars)
        pm = (
            ds["tpm10"].sel(time="20140501").sel(level=0).sum("label").mean("time")
            * 10 ** 9
        )
        img = pm.plot(
            cmap="Oranges",
            add_colorbar=False,
            vmin=0,
            vmax=50,
            # cbar_kwargs=dict(shrink=0.5, pad=0.10),
            ax=ax,
            alpha=1,
        )
        ax.set_title("")
    elif background == "OSM":
        imagery = cimgt.OSM()
        # Add the imagery to the map.
        ax.add_image(imagery, 5)

    plot_stations(df, ax=ax)

    states = cfeature.NaturalEarthFeature(
        category="cultural",
        name="admin_0_boundary_lines_land",
        scale="10m",
        facecolor="none",
    )
    # ax.background_img(name='BM_oct', resolution='highest')

    ax.coastlines(resolution="10m")
    ax.add_feature(states, edgecolor="gray")

    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=1,
        color="gray",
        alpha=0.5,
        linestyle="--",
    )
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    fig.subplots_adjust(top=0.9, bottom=0.1, left=0.1, right=0.9)
    if savefig:
        plt.savefig(DIR_FIGURES + "/stations_map.png")

    if zoom_on_stations:
        request = cimgt.OSM()
        for station in stations:
            f = plt.figure(figsize=(10, 7))
            ax = plt.subplot(projection=ccrs.PlateCarree())

            dftmp = df.loc[df["abbrv"] == station].reset_index()
            lon, lat = dftmp[["longitude", "latitude"]].values[0]
            minlon = lon - 0.5
            maxlon = lon + 0.5
            minlat = lat - 0.3
            maxlat = lat + 0.3
            ax.set_extent([minlon, maxlon, minlat, maxlat])
            plot_map(df=dftmp, pmle=pm, ax=ax, alphapm=0.6)
            ax.add_image(request, 9)
            if savefig:
                plt.savefig(DIR_FIGURES + "/map_zoom_{}.png".format(station))

    # ax.set_extent([5.4, 6.25, 45.0, 45.5]) # GRE-fr zoom
    # ax.set_extent([2.6, 3.45, 50.17, 50.80]) # LENS zoom
