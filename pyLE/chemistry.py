import pandas as pd
import itertools
import matplotlib.pyplot as plt
from matplotlib.colors import TABLEAU_COLORS, rgb2hex
import seaborn as sns
from math import sqrt
from sklearn.metrics import mean_squared_error
from pyPMF import PMF
from pyPMF.utils import get_sourceColor
from pyPMF.plotter import Plotter as PMFplotter
from py4pm.dateutilities import format_xaxis_timeseries
import statsmodels.api as sm

from .env import BASE_DIR, DIR_FIGURES, STATIONS, SITES_ORDER
from . import utils

# =================== TS Chimie    ======================================================


def _get_df_for_scatter_PMF_LE(pm10pmf, pm10le, datemin, datemax):
    datemin, datemax = utils.get_datemin_datemax(pm10pmf, pm10le, datemin, datemax)

    x = pm10pmf.loc[slice(datemin, datemax)].to_frame()
    y = pm10le.loc[slice(datemin, datemax)].resample("D").mean().to_frame()

    x.columns = ["PMF"]
    y.columns = ["LOTOS-EUROS"]
    x.index = x.index.date
    x.index.name = "Date"
    y.index.name = "Date"

    df = pd.merge(x, y, right_index=True, left_index=True)
    return df


def _get_fit_PMF_LE(df):
    """OLS fit between PMF and LE, with a constant term

    Parameters
    ----------

    df : pd.DataFrame
        Dataframe with 2 columns : "PMF" and "LOTOS-EUROS"

    Returns
    -------

    fit : statsmodels fit
    """
    fit = sm.OLS(exog=sm.add_constant(df["PMF"]), endog=df["LOTOS-EUROS"]).fit()
    return fit


def _plot_ts_PMF_LE(pm10pmf, pm10le, datemin=None, datemax=None, ax=None, twinx=False):
    """Timeserie concentration for LE and PMF

    Parameters
    ----------

    pm10pmf : pd.DataFrame
    pm10le :
    datemin : str
    datemax : str
    ax : mpl.Axes
    twinx : boolean, default False
        Plot PMF and LE on 2 twin axis

    Returns
    -------

    ax : mpl.Axes
    """
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(12, 4))
    else:
        fig = plt.gcf()

    if twinx:
        ax1 = ax
        ax2 = ax.twinx()
    else:
        ax1 = ax
        ax2 = ax

    datemin, datemax = utils.get_datemin_datemax(pm10pmf, pm10le, datemin, datemax)

    colors = list(TABLEAU_COLORS.values())
    pm10le.loc[slice(datemin, datemax)].resample("D").mean().plot(
        ax=ax1, label="LOTOS", color=colors[0]
    )
    pm10pmf.loc[slice(datemin, datemax)].plot(
        ax=ax2, marker="o", ls="-", label="PMF", color=colors[1]
    )

    h, l = ax1.get_legend_handles_labels()
    if ax1 != ax2:
        plt.setp(ax1.get_yticklabels(), color=colors[0])
        plt.setp(ax2.get_yticklabels(), color=colors[1])
        h2, l2 = ax2.get_legend_handles_labels()
        h += h2
        l += l2
    
    ax1.legend(h, l, loc="upper right")
    fig.suptitle("Daily mean contribution")
    ax1.set_ylabel("Concentration (µg m⁻³)")
    format_xaxis_timeseries(ax1)

    return ax1


def _plot_scatter_PMF_LE(
    df=None, pm10pmf=None, pm10le=None, specie=None, datemin=None, datemax=None, ax=None
):
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    if df is None:
        df = _get_df_for_scatter_PMF_LE(pm10pmf, pm10le, datemin, datemax)

    fit = _get_fit_PMF_LE(df)

    maximum = df.max().max()
    const = fit.params[0]
    a = fit.params[1]
    r2 = fit.rsquared
    # ax.set_aspect("equal")
    sns.regplot(
        data=df,
        x="PMF",
        y="LOTOS-EUROS",
        ax=ax,
        # ci=None,
        scatter=True,
    )
    ax.text(
        0.1,
        0.95,
        "{:.2f}x{:+.2f} (r²={:.2f})".format(a, const, r2),
        transform=ax.transAxes,
    )
    ax.axline(xy1=(0, 0), slope=1.0, ls="--", color="lightgrey", zorder=0)
    # ax.plot([0, maximum], [const, maximum*a+const], color="orange", linestyle="-")
    ax.axhline(0, color="lightgrey")
    ax.axvline(0, color="lightgrey")

    return (ax, fit)


def _plot_annual_source_contribution(
    dfcpmf, dfcle, datemin=None, datemax=None, axes=None
):
    if axes is None:
        fig, axes = plt.subplots(2, 1, figsize=(7, 9))

    datemin, datemax = utils.get_datemin_datemax(dfcpmf, dfcle, datemin, datemax)

    dfcle = dfcle.loc[(dfcle.index > datemin) & (dfcle.index < datemax)]
    dfcpmf = dfcpmf.loc[(dfcpmf.index > datemin) & (dfcpmf.index < datemax)]

    colors = get_sourceColor().loc["color", dfcpmf.columns]

    dfcpmf.sum().plot.pie(ax=axes[0], colors=colors)
    dfcle.sum().plot.pie(ax=axes[1])
    for ax in axes:
        ax.set_ylabel("")

    axes[0].set_title("PMF")
    axes[1].set_title("LOTOS")

    return axes


def _plot_ts_source_contribution(dfcpmf, dfcle, datemin=None, datemax=None, axes=None):
    if axes is None:
        fig, axes = plt.subplots(2, 1, figsize=(12, 7), sharex=True, sharey=True)

    datemin, datemax = utils.get_datemin_datemax(dfcpmf, dfcle, datemin, datemax)

    dfcle = (
        dfcle.loc[(dfcle.index > datemin) & (dfcle.index < datemax)]
        .resample("D")
        .mean()
        .copy()
    )
    dfcpmf = dfcpmf.loc[(dfcpmf.index > datemin) & (dfcpmf.index < datemax)].copy()

    maximum = max(dfcle.sum(axis=1).max(), dfcpmf.sum(axis=1).max())
    ymax = 1.05 * maximum
    PMFplotter._plot_ts_stackedbarplot(PMFplotter, dfcpmf, ax=axes[0])
    PMFplotter._plot_ts_stackedbarplot(PMFplotter, dfcle, ax=axes[1])
    axes[0].legend(loc="center left", bbox_to_anchor=(1.00, 0.5), frameon=False)
    axes[1].legend(loc="center left", bbox_to_anchor=(1.00, 0.5), frameon=False)
    axes[0].set_ylim(0, ymax)

    fig.subplots_adjust(top=0.90, bottom=0.10, left=0.10, right=0.85)

    return axes


def compare_pmf_le(station, pm10le=None, pmf=None, specie=None, savefig=False):
    """Compare PMF and LE outputs for both absolute sum value and source contribution

    Parameters
    ----------
    station : str
        Station to compare
    pm10le : default: None
    pmf : default: None
    specie : str, default: None
    savefig : boolean, default: False
    """
    # Get contribution by factors or labels
    dfPMF, dfLE = utils.get_station_specie_data_PMF_LE(station, specie=specie)

    # replace label numbers to labels names
    dfLE = utils.set_LE_labels_names(dfLE)

    dfPMF_sum = dfPMF.sum(axis=1)
    dfLE_sum = dfLE.sum(axis=1)

    datemin, datemax = utils.get_datemin_datemax(dfPMF_sum, dfLE_sum)
    if datemin > datemax:
        print("No date overlap between PMF and LE")
        return

    # Source contribution
    if pmf is None:
        pmf = utils.get_pmf(station)

    # Figure 1 : raw PM values
    fig, axes = plt.subplots(
        1, 2, figsize=(14, 5), gridspec_kw=dict(width_ratios=[3, 1]), sharey=False
    )
    ax_ts = _plot_ts_PMF_LE(
        dfPMF_sum, dfLE_sum, datemin=datemin, datemax=datemax, ax=axes[0]
    )
    ax_scatter, fit_scatter = _plot_scatter_PMF_LE(
        df=None,
        pm10pmf=dfPMF_sum,
        pm10le=dfLE_sum,
        specie=specie,
        datemin=datemin,
        datemax=datemax,
        ax=axes[1],
    )
    fig.suptitle("{} : {}".format(station, specie))

    if savefig:
        title = DIR_FIGURES + "/Specie_per_station/raw/{}_timeserie_raw_{}".format(
            station, specie
        )
        fig.savefig(title + ".png")

    # Figure 2 : Sources contribution
    axes_contrib = _plot_annual_source_contribution(
        dfPMF, dfLE, datemin=datemin, datemax=datemax
    )
    fig = plt.gcf()
    fig.suptitle("{} : {}".format(station, specie))

    if savefig:
        title = (
            DIR_FIGURES
            + "/Specie_per_station/contribution/{}_repartition_contribution_{}".format(
                station, specie
            )
        )
        fig.savefig(title + ".png")

    # Figure 3 : TS sources contribution
    axes_contib_ts = _plot_ts_source_contribution(
        dfPMF, dfLE, datemin=datemin, datemax=datemax
    )
    fig = plt.gcf()
    fig.suptitle("{} : {}".format(station, specie))

    if savefig:
        title = (
            DIR_FIGURES
            + "/Specie_per_station/contribution/{}_timeserie_contribution_{}".format(
                station, specie
            )
        )
        fig.savefig(title + ".png")

    return fit_scatter


def compare_pmf_le_all(stations, species, savefig=False):
    """Plot all comparision for the given stations and species

    Parameters
    ----------
    stations : list of str, default: STATIONS
        List of stations to compare
    species : list of str, default: ["PM10", "OC", "EC", "NO3-", "SO42-"]
        List of species to compare.
    """
    if stations is None:
        stations = STATIONS
    if species is None:
        species = ["PM10", "OC", "EC", "NO3-", "SO42-"]

    if savefig:
        plt.interactive(False)

    for station, specie in itertools.product(stations, species):
        print("=========== {} : {}".format(station, specie))
        try:
            compare_pmf_le(station=station, specie=specie, savefig=savefig)
        except BaseException as e:
            print(e)
        if savefig:
            plt.close("all")

    plt.interactive(True)


def get_fit_between_PMF_and_LE(stations=None, species=None, dfxy=None, pmfs=None):
    """Get a dataframe with statistical fit between PMF and LE

    Parameters
    ----------
    stations : list of str, default: None
        List of stations
    species : list of str, default: None
        List of species
    dfxy : pd.DataFrame, default: None
        If given, use this value for the fit. It should contains the following columns:
        [PMF_var, LE_var, Station, Specie] and have Date as index.
    pmfs : list of PMF, default: None
        If given, retrieve PMF from this dictionnary.
        If not given, retrieve it from the name of the station.

    Returns
    -------
    df_fit : pd.Dataframe
        DataFrame with Station and Specie as index, and ["r", "r2", "a", "const",
        "number"] as columns
    """

    df_fit = pd.DataFrame(
        index=pd.MultiIndex.from_product([stations, species]),
        columns=["r", "r2", "RMSE", "a", "const", "number"],
    )
    df_fit.index.names = ["Station", "Specie"]

    for station in stations:
        if dfxy is None:
            if pmfs is None:
                pmf = utils.get_pmf(station)
            else:
                pmf = pmfs[station]
        for specie in species:
            print("=========== {} : {}".format(station, specie))
            try:
                if dfxy is None:
                    pmf_specie, le_specie = utils.specie_names_to_LE_names(specie)
                    dfPMF = pmf.to_cubic_meter(specie=pmf_specie)  # .sum(axis=1)
                    dfLE = utils.get_data_from_le(
                        station=station, variable=le_specie, labels="all"
                    )
                    dfLE = utils.set_LE_labels_names(dfLE)
                    dfPMF_sum = dfPMF.sum(axis=1)
                    dfLE_sum = dfLE.sum(axis=1)
                else:
                    idx_pmf = (dfxy["PMF_var"]==specie) & (dfxy["Station"]==station)
                    idx_le = (dfxy["LE_var"]==specie) & (dfxy["Station"]==station)
                    dfPMF_sum = dfxy.loc[idx_pmf, "PMF"].dropna()
                    dfLE_sum = dfxy.loc[idx_pmf, "LE"].dropna()

                df = _get_df_for_scatter_PMF_LE(dfPMF_sum, dfLE_sum, None, None)

                fit = _get_fit_PMF_LE(df)

                mse = mean_squared_error(fit.model.endog, fit.fittedvalues)
                rmse = sqrt(mse)
                df_fit.loc[(station, specie)] = [
                    sqrt(fit.rsquared),
                    fit.rsquared,
                    rmse,
                    fit.params[1],
                    fit.params[0],
                    fit.nobs,
                ]
            except BaseException as e:
                print(e)
    return df_fit


def aggregate_comparison_all_stations_species(
    stations=None, species=None, df_fit=None, savefig=False, figtitle=None, figname=None
):
    """Set a aggregate comparison of all stations and species between PMF and LE.

    Parameters
    ----------
    stations : list of str, STATIONS
        List of stations to compare
    species : list of str
        List of species to compare, default to ["PM10", "OC", "EC", "NO3-", "SO42-", "Na+"]
    df_fit : pd.DataFrame, None
        Dataframe with Station and Specie as index, and ["r", "r2", "a", "const",
        "number"] as columns.
    savefig : boolean, False
        Save or not the figure
    figtitle : str, default None
        Suptitle of the figure
    figname : str, default None
        Name of the file saved if savefig is True

    Returns
    -------

    df_fit : pd.DataFrame
        Dataframe with Station and Specie as index, and ["r", "r2", "a", "const", "number"] as columns.
    """

    def _hacky_stemplot(df_fit, col, species, stations, sites_order, ax):
        # Hacky stuf for the grouped stemplot...
        # First, draw a barplot with seaborn, then use the bar to plot the stem
        sns.barplot(
            data=df_fit.reset_index(),
            x="Station",
            y=col,
            hue="Specie",
            # join=False,
            order=sites_order,
            ax=ax,
        )

        patches_group = []
        for i in range(0, len(species)):
            start = len(stations)*i
            end = start + len(stations)
            patches_group.append(ax.patches[start:end])

        if col == "a":
            for i in range(0, len(stations)):
                ax.plot([i-0.333, i+0.333], [1, 1], "-", lw=1, color="grey")
        ax.axhline(0, ls="-", color="black", lw=1)

        for i, patches in enumerate(patches_group):
            x = [p.get_x()+p.get_width()/2 for p in patches]
            y = [p.get_height() for p in patches]
            color = rgb2hex(patches[0].get_facecolor())
            markerline, stemlines, baseline = ax.stem(x, y, bottom={"a": 1, "const": 0}[col])
            markerline.set_markerfacecolor(color)
            markerline.set_markeredgecolor(color)
            stemlines.set_color("grey")
            baseline.remove()
            [p.remove() for p in patches]
        ax.set_xlim(-0.5, len(stations)-0.5)

    if stations is None:
        stations = STATIONS
    if species is None:
        species = ["PM10", "OC", "EC", "NO3-", "SO42-", "Na+"]

    if df_fit is None:
        df_fit = get_fit_between_PMF_and_LE(stations, species)
    else:
        df_fit = df_fit.loc[(stations, species), :]

    sites_order = [x for x in SITES_ORDER if x in stations]

    # ==== r2 plot
    fig, axes = plt.subplots(3, 1, figsize=(12, 8))
    sns.barplot(
        data=df_fit.reset_index(),
        x="Station",
        y="r2",
        hue="Specie",
        # join=False,
        order=sites_order,
        ax=axes[0],
    )

    # ==== a plot
    _hacky_stemplot(df_fit=df_fit,
            col="a",
            species=species,
            stations=stations,
            sites_order=sites_order,
            ax=axes[1]
            )
    # ==== intercept plot
    _hacky_stemplot(df_fit=df_fit,
            col="const",
            species=species,
            stations=stations,
            sites_order=sites_order,
            ax=axes[2]
            )


    handles, labels = axes[0].get_legend_handles_labels()
    axes[0].set_ylim(0, 1)
    axes[0].legend().remove()
    axes[1].legend().remove()
    axes[2].legend().remove()
    axes[0].set_xlabel("")
    axes[1].set_xlabel("")

    utils.plot_add_background(axes[0], text=True, addBackground=True)
    utils.plot_add_background(axes[1], text=False, addBackground=True)
    utils.plot_add_background(axes[2], text=False, addBackground=True)

    fig.subplots_adjust(left=0.05)
    fig.legend(handles, labels, loc="center right", frameon=False)

    if figtitle:
        fig.suptitle(figtitle)

    if savefig:
        if figname:
            figname = DIR_FIGURES + "/" + figname
        else:
            figname = DIR_FIGURES + "/correlation_LOTOS-PMF"
        fig.savefig(figname + ".png")

    return df_fit


def fit_to_table_per_specie(df_fit):
    """Return values of fit formatted for each station and specie

    Parameters
    ----------

    df_fit : pd.DataFrame
        Dataframe of [Station, Specie] MultiIndex, and "a", "const" and "r2" columns.

    Returns
    -------

    df : pd.DataFrame
    """

    stations = df_fit.index.get_level_values("Station").unique()
    species = df_fit.index.get_level_values("Specie").unique()
    fmt = "{:.2f}x {:+.2f} (r²={:.2f}, RMSE={:.2f})"

    df = pd.DataFrame(
        columns=["PM10", "OC", "EC", "NO3-", "SO42-", "Na+", "#"], index=stations
    )
    df.loc[stations, "#"] = df_fit.loc[(slice(None), "PM10"), "number"].reset_index(
        "Specie", drop=True
    )
    for station, specie in itertools.product(stations, species):
        df.loc[station, specie] = fmt.format(
            df_fit.loc[(station, specie), "a"],
            df_fit.loc[(station, specie), "const"],
            df_fit.loc[(station, specie), "r2"],
            df_fit.loc[(station, specie), "RMSE"],
        )

    sites_order = [x for x in SITES_ORDER if x in stations]
    df = df.loc[sites_order]
    return df
