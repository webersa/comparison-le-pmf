# =================== OP inversion ======================================================
class Solver:
    def __init__(self, X=None, y=None, sigma=None, NBOOT=100, rejection=None):
        self.endog = y
        self.exog = sm.add_constant(X.copy())
        self.sigma = sigma
        self.weight = 1 / (sigma ** 2)
        self.NBOOT = NBOOT
        self.rejection = rejection

        self.BSreg = []
        self.BSpredict_testing = pd.DataFrame(columns=["test", "predict", "Date", "BS"])
        self.reg = None

    def WLS(self, bootstrap=False, X=None, y=None, weight=None):

        if bootstrap:
            if (
                isinstance(X, pd.DataFrame)
                and isinstance(y, pd.Series)
                and isinstance(weight, pd.Series)
            ):
                endog = y
                exog = X
        else:
            endog = self.endog
            exog = self.exog
            weight = self.weight

        if self.rejection == "pval":
            while True:
                reg = sm.WLS(endog, exog, weights=weight, cov_type="fixed_scale").fit()
                if (reg.pvalues < 0.05).all():
                    break
                else:
                    if len(exog.columns) == 0:
                        # no more value...
                        logger.warning("No more variable...")
                        break
                    var = reg.pvalues.idxmax()
                    logger.debug("Removing variable {}".format(var))
                    exog.drop(var, inplace=True, axis=1)
                    reg = sm.WLS(
                        endog, exog, weights=weight, cov_type="fixed_scale"
                    ).fit()

        else:
            reg = sm.WLS(endog, exog, weights=weight, cov_type="fixed_scale").fit()

        self.reg = reg
        return reg

    def bootstrap(self, train_size=0.7):
        idx = self.endog.index
        self.BSreg = list()
        for bs in range(self.NBOOT):
            logger.debug("BOOTSTRAP{}".format(bs))
            train_idx = random.sample(list(idx), int(train_size * len(idx)))
            test_idx = idx.difference(train_idx)
            X_train = self.exog.loc[train_idx]
            X_test = self.exog.loc[test_idx]
            y_train = self.endog.loc[train_idx]
            y_test = self.endog.loc[test_idx]
            weight_train = self.weight.loc[train_idx]
            weight_test = self.weight.loc[test_idx]
            # get the regression
            reg = self.WLS(X=X_train, y=y_train, weight=weight_train, bootstrap=True)
            # predict
            y_predict = (reg.params * X_test).sum(axis=1)
            # storage
            self.BSreg.append(reg)
            self.BSpredict_testing = self.BSpredict_testing.append(
                pd.DataFrame(
                    data={
                        "test": y_test.values,
                        "predict": y_predict.values,
                        "Date": y_test.index.get_level_values("Date"),
                        "BS": bs,
                    }
                )
            )

    def _get_all_OPi_from_bootstrap(self):
        """get all OPi from the bootstrap

        :returns: pd.DataFrame

        """
        OPi = pd.DataFrame({i: reg.params for i, reg in enumerate(self.BSreg)})
        return OPi

    def _get_all_prediction_from_bootstrap(self):
        """get all OPi from the bootstrap

        :returns: pd.DataFrame

        """
        pred = pd.DataFrame(
            {i: reg.predict(exog=self.exog) for i, reg in enumerate(self.BSreg)}
        )
        return pred


def _compute_OPi(station, OPtype):

    df = get_PM10_and_OP([station])
    SRC = df[
        list(
            set(df.columns)
            - set(["OP_AA_m3", "OP_DTT_m3", "SD_OP_AA_m3", "SD_OP_DTT_m3"])
        )
    ]
    OPunc = df["SD_" + OPtype]
    OP = df[OPtype]

    # Initialize solver
    solver = Solver(X=SRC, y=OP, sigma=OPunc, NBOOT=NBOOT, rejection=None)

    # solve WLS
    solver.WLS()

    # Bootstrap solution
    solver.bootstrap()

    return solver


def inversion_OP_on_LE_sources(stations, OPtype, modeltype="ols", split=False):
    """

    Parameters
    ==========

    stations : list of str
    OPtype : str
    modeltype : str, ols or wls
    split : split in train and test dataframe

    Returns
    =======

    dftrain : training dataframe
    dftest : testing dataframe
    fit : regression fit (from statsmodels)
    """
    df = get_PM10_and_OP(stations)

    if split:
        dftrain, dftest = train_test_split(df)
    else:
        dftrain = df
        dftest = pd.DataFrame()

    if modeltype == "ols":
        model = smf.ols(
            "{OPtype} ~ {sources}".format(
                OPtype=OPtype, sources=" + ".join(LABELS.keys())
            ),
            data=dftrain,
        )
    elif modeltype == "wls":
        model = smf.wls(
            "{OPtype} ~ {sources}".format(
                OPtype=OPtype, sources=" + ".join(LABELS.keys())
            ),
            data=dftrain,
            weight=1 / (dftrain["SD_" + OPtype] ** 2),
            cov_type="fixed_scale",
        )

    fit = model.fit()

    return (dftrain, dftest, fit)


def get_result_per_station(stations):

    optypes = ["OP_DTT_m3", "OP_AA_m3"]

    dffit = pd.DataFrame(index=stations, columns=optypes)

    for station, optype in itertools.product(stations, optypes):
        try:
            _, _, fit = inversion_OP_on_LE_sources([station], optype)
            dffit.loc[station, optype] = fit
        except:
            pass

    df = pd.DataFrame(
        index=pd.MultiIndex.from_product([stations, fit.params.index]),
        columns=optypes + ["SD_" + optype for optype in optypes],
    )
    df.index.names = ["Station", "Source"]
    for station, optype in itertools.product(stations, optypes):
        try:
            df.loc[station, optype] = dffit.loc[station, optype].params.values
            df.loc[station, "SD_" + optype] = dffit.loc[station, optype].bse.values
        except:
            pass

    intercept = df.loc[(slice(None), "Intercept"), :].copy()
    df = df.loc[(slice(None), list(set(fit.params.index) - set(["Intercept"]))), :]
    df = df.reset_index().dropna()
    df = df.pivot(index=["Source"], columns=["Station"])

    fig, axes = plt.subplots(2, 1, figsize=(12, 8))

    df["OP_DTT_m3"].plot(kind="bar", yerr=df["SD_OP_DTT_m3"], ax=axes[0])
    df["OP_AA_m3"].plot(kind="bar", yerr=df["SD_OP_AA_m3"], ax=axes[1])
    return (dffit, df, intercept)


def plot_inversion_result(dftrain, dftest, res, OPtype="OP_DTT_m3"):

    fig, axes = plt.subplots(
        2,
        2,
        # sharex="col",
        # sharey="row",
        figsize=(16, 7),
        gridspec_kw=dict(width_ratios=[3, 1]),
    )

    pred_vs_obs = pd.DataFrame(index=res.resid.index)
    pred_vs_obs["prediction"] = res.predict()
    pred_vs_obs["observation"] = res.model.endog
    pred_vs_obs = pred_vs_obs.reset_index()

    res_vs_obs = pd.DataFrame(index=res.resid.index)
    res_vs_obs["residual"] = res.resid
    res_vs_obs["observation"] = res.model.endog
    res_vs_obs = res_vs_obs.reset_index()
    resid = res.resid.reset_index()
    resid.columns = ["Station", "Date", "residual"]
    # Fit part =========================
    sns.lineplot(
        data=pred_vs_obs.melt(id_vars=["Date", "Station"], value_name="OP"),
        x="Date",
        y="OP",
        hue="Station",
        style="variable",
        markers=True,
        ax=axes[0, 0],
    )
    sns.scatterplot(
        data=pred_vs_obs,
        x="observation",
        y="prediction",
        hue="Station",
        markers=True,
        ax=axes[0, 1],
    )

    # Resid part =======================
    sns.lineplot(
        data=resid,
        x="Date",
        y="residual",
        hue="Station",
        marker="o",
        ax=axes[1, 0],
        legend=True,
    )

    sns.scatterplot(
        data=res_vs_obs, x="observation", y="residual", hue="Station", ax=axes[1, 1]
    )

    format_xaxis_timeseries(axes[0, 0])
    format_xaxis_timeseries(axes[1, 0])

    ylim = max(res.model.endog) + 1
    axes[0, 0].set_ylim(0, ylim)
    axes[0, 1].set_ylim(0, ylim)
    axes[0, 1].set_xlim(0, ylim)
    axes[0, 1].plot([0, ylim], [0, ylim], ls="--", color="lightgrey")

    ylim = max(abs(resid["residual"].min()), abs(resid["residual"].max())) + 0.2
    axes[1, 0].set_ylim(-ylim, ylim)
    axes[1, 0].axhline(0, ls="--", color="lightgrey")
    axes[1, 1].set_ylim(-ylim, ylim)
    axes[1, 1].axhline(0, ls="--", color="lightgrey")

    for ax in axes.flatten():
        ax.legend(loc="center left", bbox_to_anchor=(1.00, 0.5), frameon=False)

    fig.subplots_adjust(left=0.05, right=0.9, wspace=0.3)

    fig, ax = plt.subplots(1, 1, figsize=(6, 4))
    sources = list(LABELS.keys())
    OPi = res.params[sources].sort_values()
    OPi_unc = res.bse[OPi.index]
    OPi.plot(kind="barh", xerr=OPi_unc, ax=ax)
    fig.subplots_adjust(top=0.9, bottom=0.14, left=0.2, right=0.9)
    ax.set_xlabel(f"Intrinsic OP {OPtype} (nmol min⁻¹ µg⁻¹)")


def plot_all_station_reg_OP(dffit, OPtype="OP_DTT_m3"):

    for station in dffit.index:
        reg = dffit.loc[station, OPtype]
        try:
            plot_inversion_result(None, None, reg, OPtype=OPtype)
        except BaseException as e:
            print(e)
