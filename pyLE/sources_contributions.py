import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.lines as mlines
from matplotlib.colors import TABLEAU_COLORS
import seaborn as sns

from pyLE import utils, env, chemistry


"""
Module use to compare source contribution one by one
"""


def plot_regplot(df, ax):
    """ """
    colors = list(TABLEAU_COLORS.values())

    # Not efficient, but needed to always plot it in the same order...
    i = 0
    stationsInTypo = df["Station"].unique()
    typo = utils.get_station_typology(stationsInTypo[0])
    for station in env.SITES_ORDER:
        if station in stationsInTypo:
            dfs = df.loc[df["Station"] == station]
            sns.regplot(
                data=dfs,
                x="PMF",
                y="LE",
                ax=ax,
                color=colors[i],
                marker=env.TYPOLOGIES[typo]["marker"],
            )
            i += 1

    ax.axline(xy1=(0, 0), slope=1.0, ls="--", color="lightgrey", zorder=0)


def timeserie_stations_PMF_vs_LE(df, stations, title=None, savefig=False, ts_kws={}):
    """
    Parameters
    ----------

    df : pd.DataFrame
        DataFrame with ["Station", "PMF", "LE"] columns
    stations : list of str
        List of stations to plot
    title : str
        Title of the figure
    savefig : boolean, default: False
        Save the figure as png
    """

    for station in stations:
        dftmp = df.loc[df["Station"] == station].set_index("Date")
        x = dftmp["PMF"].dropna()
        y = dftmp["LE"].dropna()

        if x.size == 0 or y.size == 0:
            continue

        fig, axes = plt.subplots(
            1, 2, figsize=(14, 4), gridspec_kw=dict(width_ratios=[3, 1]), sharey=False
        )
        ax_ts = chemistry._plot_ts_PMF_LE(x, y, ax=axes[0], **ts_kws)
        ax_scatter, fit_scatter = chemistry._plot_scatter_PMF_LE(
            df=None,
            pm10pmf=x,
            pm10le=y,
            ax=axes[1],
        )
        fig.suptitle("{} : {}".format(station, title))

        fig.subplots_adjust(
            top=0.88, bottom=0.11, left=0.07, right=0.98, hspace=0.2, wspace=0.22
        )

        if savefig:
            name = (
                env.DIR_FIGURES
                + f"/per_sources/timeserie_{station}_{title.replace(' ', '_').replace('/', '_')}"
            )
            plt.savefig(name + ".png")


def scatterplot_stations_PMF_vs_LE(df, stations, title=None, savefig=False, hue=None):
    """
    Parameters
    ----------

    df : pd.DataFrame
        DataFrame with ["Station", "PMF", "LE", "Typology"] columns
    stations : list of str
        List of stations to plot
    title : str
        Title of the figure
    savefig : boolean, default: False
        Save the figure as png
    """
    fig, ax = plt.subplots(figsize=(7, 5), ncols=1, sharex=True, sharey=True)
    if hue == "Station":
        for i, typo in enumerate(
            ["Remote", "Urban", "Urban valley", "Industrial", "Traffic"]
        ):
            dft = df.loc[df["Typology"] == typo]
            plot_regplot(dft, ax=ax)
        utils.legend_by_typology(fig, stations)
    else:
        sns.regplot(data=df, x="PMF", y="LE", ax=ax)
        ax.axline(xy1=(0, 0), slope=1.0, ls="--", color="lightgrey", zorder=0)

    ax.set_ylabel("LOTOS-EUROS (µg/m³)")
    ax.set_xlabel("PMF (µg/m³)")
    fig.subplots_adjust(top=0.896, bottom=0.118, left=0.118, right=0.80)

    if title:
        fig.suptitle(title)
    if savefig:
        name = env.DIR_FIGURES + "/per_sources/scatterplot_{title}{hued}".format(
            title=title.replace(" ", "_").replace("/", "_"),
            hued="_hue_station" if hue else "",
        )
        plt.savefig(name + ".png")


def plot_PMF_vs_LE(df, title=None, savefig=False, ts_kws={}):
    """
    Parameters
    ----------

    df : pd.DataFrame
        DataFrame with ["Station", "PMF", "LE", "Typology"] columns
    title : str
        Title of the figure
    savefig : boolean, default: False
        Save the figure as png
    """

    stations = df["Station"].unique()

    timeserie_stations_PMF_vs_LE(
        df, stations, title=title, savefig=savefig, ts_kws=ts_kws
    )

    scatterplot_stations_PMF_vs_LE(df, stations, title=title, savefig=savefig)
    scatterplot_stations_PMF_vs_LE(
        df, stations, title=title, savefig=savefig, hue="Station"
    )


def _get_source_contribution_for_specie(
    stations, PMF_factors, LE_labels, PMF_specie, LE_specie, pmfs=None
):
    """Get a dataframe with source contribution from PMF and LE for the given factor and
    label and specie

    Parameters
    ----------

    stations : list of str
    PMF_specie : str
    LE_specie : str
    PMF_factors : str
        PMF factor name to get
    LE_labels : list of str
        Lotos labels name to get
    pmfs : dict of PMF, default: None
        Dictionarry of PMF object. If None, all PMF for the stations given will be
        retrieve

    Returns
    -------

    df : pd.DataFrame

    """
    dfPMF = utils.get_stations_contribution_from_pmf(
        stations=stations, factors=PMF_factors, specie=PMF_specie, pmfs=pmfs
    )
    dfPMF.columns = ["PMF"]

    dfLE = utils.get_stations_contribution_from_le(
        stations=stations, specie=LE_specie, labels=LE_labels
    )
    dfLE.columns = ["LE"]

    df = pd.merge(dfPMF, dfLE, left_index=True, right_index=True, how="outer")
    df.index.names = ["Station", "Date"]

    df = df.reset_index()
    df["PMF_var"] = PMF_specie
    df["LE_var"] = LE_specie

    df["Typology"] = df["Station"].apply(utils.get_station_typology)

    return df


def _get_source_contribution_for_species(
    stations, PMF_species, LE_species, PMF_factors, LE_labels, pmfs=None
):
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)

    df_dict = {}
    for PMF_specie, LE_specie in zip(PMF_species, LE_species):
        specie = f"{PMF_specie}--{LE_specie}"
        df_dict[specie] = _get_source_contribution_for_specie(
            stations=stations,
            PMF_specie=PMF_specie,
            LE_specie=LE_specie,
            PMF_factors=PMF_factors,
            LE_labels=LE_labels,
            pmfs=pmfs,
        )

    df = pd.concat(df_dict, names=["Specie"]).reset_index().drop("level_1", axis=1)
    return df


def plot_BB(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot BB timeseries comparison for the given stations

    Default comparison are : PM10, OC, EC for Biomass_burning PMF and Biomass LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["PM10", "OC", "EC"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=species,
        PMF_factors=["Biomass_burning"],
        LE_labels=["Biomass"],
        pmfs=pmfs,
    )

    dffit = chemistry.get_fit_between_PMF_and_LE(
        stations=stations, dfxy=df.set_index("Date"), species=species
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["Specie"] == f"{specie}--{specie}"]
            plot_PMF_vs_LE(dftmp, title=f"Biomass burning {specie}", savefig=savefig)

    chemistry.aggregate_comparison_all_stations_species(
        stations=stations,
        species=species,
        df_fit=dffit,
        savefig=savefig,
        figtitle="Biomass burning PMF vs Biomass LOTOS",
        figname="per_sources/aggregate_biomassburning",
    )

    return (df, dffit)


def plot_Traffic(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot Traffic timeseries comparison for the given stations

    Default comparison are : PM10, OC, EC for Road Traffic PMF and RoadTrans LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["PM10", "OC", "EC"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=species,
        PMF_factors=["Road traffic"],
        LE_labels=["RoadTrans"],
        pmfs=pmfs,
    )

    dffit = chemistry.get_fit_between_PMF_and_LE(
        stations=stations, dfxy=df.set_index("Date"), species=species
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["Specie"] == f"{specie}--{specie}"]
        plot_PMF_vs_LE(dftmp, title=f"Traffic {specie}", savefig=savefig)

    chemistry.aggregate_comparison_all_stations_species(
        stations=stations,
        species=species,
        df_fit=dffit,
        savefig=savefig,
        figtitle="Traffic PMF vs RoadTrans LOTOS",
        figname="per_sources/aggregate_traffic",
    )

    return (df, dffit)


def plot_NonRoadTraffic(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot NonRoadTraffic timeseries comparison for the given stations

    Default comparison are : V for Traffic PMF and PPM for NonRoadTraffic LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["V"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=["ppm"],
        PMF_factors="all", #["HFO", "Marine/HFO"],  # ["Road traffic"],
        LE_labels=["NonRoadTrans"],
        pmfs=pmfs,
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["Specie"] == "V--ppm"]
            plot_PMF_vs_LE(
                dftmp,
                title="V from PMF all factors vs ppm from LOTOS NonRoadTrans",
                ts_kws=dict(twinx=True),
                savefig=savefig,
            )

    return df


def plot_SeaSalt(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot SeaSalt timeseries comparison for the given stations

    Default comparison are : PM10, Na+ for [Aged salt + Salt] PMF and tss, Na+ LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["PM10", "Na+"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=["tss", "Na+"],
        PMF_factors=["Aged_salt", "Salt"],
        LE_labels="all",
        pmfs=pmfs,
    )

    df.loc[df["LE_var"] == "tss", "LE_var"] = "PM10"

    dffit = chemistry.get_fit_between_PMF_and_LE(
        stations=stations, dfxy=df.set_index("Date"), species=species
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["PMF_var"] == specie]
            plot_PMF_vs_LE(
                dftmp,
                title=f"{specie} PMF sea salt (fresh + aged) vs {specie} LOTOS tss",
                savefig=savefig,
            )

    chemistry.aggregate_comparison_all_stations_species(
        stations=stations,
        species=species,
        df_fit=dffit,
        savefig=savefig,
        figtitle="Salt + Aged salt PMF vs tss LOTOS",
        figname="per_sources/aggregate_seasalt",
    )

    return (df, dffit)


def plot_Dust(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot Dust timeseries comparison for the given stations

    Default comparison are : PM10 for Dust PMF and tdust LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["PM10"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=["tdust"],
        PMF_factors=["Dust"],
        LE_labels="all",
        pmfs=pmfs,
    )

    df.loc[df["LE_var"] == "tdust", "LE_var"] = "PM10"

    dffit = chemistry.get_fit_between_PMF_and_LE(
        stations=stations, dfxy=df.set_index("Date"), species=species
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["PMF_var"] == specie]
            plot_PMF_vs_LE(
                dftmp,
                title=f"{specie} PMF Dust vs {specie} LOTOS tdust",
                savefig=savefig,
            )

    chemistry.aggregate_comparison_all_stations_species(
        stations=stations,
        species=species,
        df_fit=dffit,
        savefig=savefig,
        figtitle="Dust PMF vs tdust LOTOS",
        figname="per_sources/aggregate_dust",
    )

    return (df, dffit)

def plot_PBOA(stations, species=None, pmfs=None, per_station=True, savefig=False):
    """Plot Dust timeseries comparison for the given stations

    Default comparison are : PM10 for Dust PMF and tdust LE

    Parameters
    ----------

    stations : list of str
    species : list of str
    pmfs : dict of PMF object
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)
    if species is None:
        species = ["OC"]

    df = _get_source_contribution_for_species(
        stations=stations,
        PMF_species=species,
        LE_species=["ppm"],
        PMF_factors=["Primary_biogenic"],
        LE_labels=["Agriculture"],
        pmfs=pmfs,
    )

    dffit = chemistry.get_fit_between_PMF_and_LE(
        stations=stations, dfxy=df.set_index("Date"), species=species
    )

    if per_station:
        for specie in species:
            dftmp = df.loc[df["PMF_var"] == specie]
            plot_PMF_vs_LE(
                dftmp,
                title=f"{specie} PMF PBOA vs ppm LOTOS agriculture",
                savefig=savefig,
                ts_kws=dict(twinx=True),
            )

    chemistry.aggregate_comparison_all_stations_species(
        stations=stations,
        species=species,
        df_fit=dffit,
        savefig=savefig,
        figtitle="OC PBOA PMF vs ppm agriculture LOTOS",
        figname="per_sources/aggregate_pboa",
    )

    return (df, dffit)


def plot_all_sources_comparison(stations, pmfs=None, per_station=True, savefig=False):
    """Plot all the plot_* things with default comparison (see indivual function)

    Parameters
    ----------

    stations : list of str
    pmfs : dict of PMF
    per_station : boolean, default: True
    savefig : boolean, default: False
    """
    if pmfs is None:
        pmfs = utils.get_pmfs(stations)

    if savefig:
        plt.interactive(False)

    plot_BB(stations, pmfs=pmfs, per_station=per_station, savefig=savefig)
    plot_Traffic(stations, pmfs=pmfs, per_station=per_station, savefig=savefig)
    plot_NonRoadTraffic(stations, pmfs=pmfs, per_station=per_station, savefig=savefig)
    plot_SeaSalt(stations, pmfs=pmfs, per_station=per_station, savefig=savefig)
    plot_Dust(stations, pmfs=pmfs, per_station=per_station, savefig=savefig)

    plt.interactive(True)
