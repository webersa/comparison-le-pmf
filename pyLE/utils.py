import os
import numpy as np
import pandas as pd
import xarray as xr
from datetime import datetime as dt
import sqlite3
from matplotlib.patches import Rectangle
import matplotlib.lines as mlines
import matplotlib.text as mtext
from matplotlib.colors import TABLEAU_COLORS
import collections
from pyPMF import PMF

from .env import (
    DIR_LE_DATA,
    DIR_LE_PER_STATION,
    DB_PMF,
    LE_VARIABLES,
    STATIONS_SHORT2LONG,
    STATIONS_LONG2SHORT,
    LE_LABELS,
    LE_NETCDF_FILENAME,
    TYPOLOGIES,
    STATIONS,
)


class LegendTitle(object):
    """Utility to print blocks of legend"""

    def __init__(self, text_props=None):
        self.text_props = text_props or {}
        super(LegendTitle, self).__init__()

    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        title = mtext.Text(x0, y0, orig_handle, **self.text_props)
        handlebox.add_artist(title)
        return title


def legend_by_typology(fig, stations):
    """
    Parameters
    ----------

    fig : mpl.Figure
    stations : list of str
    """
    # create custom legend
    labels = []
    artists = []
    for typo in TYPOLOGIES.keys():
        colors = list(TABLEAU_COLORS.values())
        j = 0
        for site in TYPOLOGIES[typo]["site"]:
            if site not in stations:
                continue
            if j == 0:
                artist = typo
                label = ""
                artists.append(artist)
                labels.append(label)

            artist = mlines.Line2D(
                [],
                [],
                ls="",
                marker=TYPOLOGIES[typo]["marker"],
                color=colors[j],
                label=site,
            )
            label = site
            artists.append(artist)
            labels.append(label)
            j += 1

    fig.legend(
        artists,
        labels,
        loc="center right",
        handler_map={str: LegendTitle()},
        frameon=False,
    )


def plot_common_date_pmf_op(pmfs=None):
    """Plot common date between PMF, OP and LE

    Parameters
    ----------

    pmfs : list, None
        List of PMF object to use. If None, retrieve all STATIONS.
    """

    dfOP = pd.read_sql(
        """
    SELECT * FROM values_all WHERE
    Station IN ('{stations}') AND
    OP_AA_m3 NOT NULL
    """.format(
            stations="', '".join(STATIONS_OP)
        ),
        con=sqlite3.connect(BDD_AEROSOLS),
        parse_dates=["Date"],
    )
    dfOP = dfOP.loc[dfOP["Date"] > "2012"]
    dfOP = dfOP.loc[dfOP["Date"] < "2017"]
    dfOP["Station"] = dfOP["Station"].replace(LONG2SHORT)
    dfOP.set_index(["Station", "Date"], inplace=True)

    if pmfs is None:
        pmfs = get_pmfs(STATIONS)
    dfPMF = pd.concat({k: pmf.dfcontrib_c for k, pmf in pmfs.items()})
    dfPMF = dfPMF.reset_index().set_index(["level_0", "Date"])
    dfPMF.index.names = ["Station", "Date"]

    stations = set(dfOP.index.get_level_values("Station")) | set(
        dfPMF.index.get_level_values("Station")
    )

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 8))
    colors = itertools.cycle(mpl.colors.TABLEAU_COLORS.values())
    for i, station in enumerate(stations):
        color = next(colors)
        plot_ops = dict(alpha=0.5, color=color, label=station)
        try:
            date = dfOP.loc[station].index
            ax.plot(date, [i - 0.2] * len(date), "-o", **plot_ops)
        except:
            pass
        try:
            date = dfPMF.loc[station].index
            ax.plot(date, [i + 0.2] * len(date), "-x", **plot_ops)
        except:
            pass

    ax.set_yticks(range(len(stations)))
    ax.set_ylim(-0.5, len(stations) - 0.5)
    ax.set_yticklabels(stations)
    ax.xaxis.set_major_locator(mdates.YearLocator())


def specie_names_to_LE_names(specie):
    """Map PMF species names to LE variables names

    Parameters
    ----------

    specie : str

    Returns
    -------

    pmf_specie : str
    le_specie : str or list of str
    """
    if specie is None or specie == "PM10":
        specie = "PM10"
        pmf_specie = "PM10"
        le_specie = "tpm10"
    elif specie == "SO42-":
        pmf_specie = "SO42-"
        le_specie = ["so4a_c", "so4a_f"]
    elif specie == "NO3-":
        pmf_specie = "NO3-"
        le_specie = ["no3a_c", "no3a_f"]
    elif specie == "EC":
        pmf_specie = "EC"
        le_specie = ["ec_c", "ec_f"]
    elif specie == "OC":
        pmf_specie = "OC"
        le_specie = "oc"
    elif specie == "Na+":
        pmf_specie = "Na+"
        le_specie = "na"
    elif specie == "ppm":
        pmf_specie = "PM10"  # not much sens...
        le_specie = ["ppm_c", "ppm_f"]
    else:
        pmf_specie = specie
        le_specie = specie

    return (pmf_specie, le_specie)


def get_station_typology(station):
    """Return the typology of the site based on TYPOLOGIES

    Parameters
    ----------

    station : str

    Returns
    -------

    typology : str
    """

    for k, v in TYPOLOGIES.items():
        if station in v["site"]:
            return k
    return None


def get_typology_color(station):
    """Return the color of the site typology based on TYPOLOGIES

    Parameters
    ----------

    station : str

    Returns
    -------

    color : str
    """

    for k, v in TYPOLOGIES.items():
        if station in v["site"]:
            return v["color"]
    return "black"


def get_count_by_typology(stations):
    """Count the amount of stations belonging to the sites typologies

    Parameters
    ----------

    stations : list of str

    Returns
    -------

    n : dict
        Dict TYPOLOGIES.keys() {Traffix: X, etc}, with X a integer
    """

    n = collections.OrderedDict()
    for k in TYPOLOGIES.keys():
        n[k] = 0

    for station in stations:
        typo = get_station_typology(station)
        n[typo] += 1
    return n


def get_datemin_datemax(df1, df2, datemin=None, datemax=None):
    """Get the minimum and maximum commons date of 2 dataframe

    Parameters
    ----------

    df1, df2 : pd.DataFrame
        Dataframe with DateTimeIndex as index
    datemin : datetime, default None
    datemax : datetime, default None

    Returns
    -------

    tuple : (datemin, datemax)
    """
    if datemin is None:
        datemin = pd.to_datetime(max(df1.index.min(), df2.index.min()))
    if datemax is None:
        datemax = pd.to_datetime(min(df1.index.max(), df2.index.max()))
    if datemin > datemax:
        print("No date overlap between PMF and LE")
    return (datemin, datemax)


def get_station_coordinates(stations):
    """Get lon, lat of stations

    Parameters
    ----------

    stations : list of str

    Returns
    -------

    df : pd.DataFrame
    """

    query = """
    SELECT * FROM metadata_station 
    WHERE abbrv IN ('{stations}')
    OR name IN ('{stations}')
    """.format(
        stations="', '".join(stations)
    )
    df = pd.read_sql(query, con=sqlite3.connect(DB_PMF))

    return df


def get_pmf(station):
    """Get the PMF result of the given station

    Parameters
    ----------

    station : str
        The station to retrieve

    Returns
    -------

    pmf : pyPMF.PMF
        The PMF object.
    """
    pmf = PMF(
        site=station,
        reader="sql",
        SQL_connection=sqlite3.connect(DB_PMF),
        SQL_program="SOURCES",
    )
    pmf.read.read_all()
    pmf.rename_factors_to_factors_category()
    pmf.rename_factors({"MSA_rich": "Marine SOA"})
    # In PMF, we have OC* (i.e. OC minus organic carbon mass already determined (levo, etc))
    pmf.recompute_new_species("OC")

    return pmf


def get_pmfs(stations):
    """Get PMF object for the given stations

    Parameters
    ----------

    stations : list of str
        Name of the station to extract from the PMF database

    Returns
    -------

    pmfs : dict
        Dictionnary of PMF object, with station name as key.
    """
    pmfs = {}
    for station in stations:
        pmf = get_pmf(station)
        pmfs[station] = pmf
    return pmfs


def get_OP(stations):
    """Get OP values for the given stations

    Parameters
    ----------

    stations : list of str

    Returns
    -------

    df : pd.DataFrame
        Value of OP indexed by station and date
    """
    variables = [
        "Station",
        "Date",
        "OP_AA_m3",
        "OP_DTT_m3",
        "SD_OP_AA_m3",
        "SD_OP_DTT_m3",
    ]
    query = "SELECT {variables} FROM OP WHERE Station IN ('{stations}')".format(
        variables=", ".join(variables),
        stations="', '".join(
            [STATIONS_SHORT2LONG.get(station, station) for station in stations]
        ),
    )
    df = pd.read_sql(
        query,
        con=sqlite3.connect(DB_PMF),
        parse_dates=["Date"],
    )

    df["Station"] = df["Station"].replace(STATIONS_LONG2SHORT)

    # ensure date has no hour
    df["Date"] = df["Date"].apply(lambda x: x.date())

    return df


def get_PM10_and_OP(stations):
    """Get PM10 from LE and OP for the given stations

    Parameters
    ----------

    stations : list of str

    Returns
    -------

    df : pd.DataFrame
        Value of PM10 from LE and OP for the station, indexed by Stations and Date
    """
    # Ensure station are the abbreviation, not the long name
    stations = [STATIONS_LONG2SHORT.get(x, x) for x in stations]

    # get OP data
    dfOP = get_OP(stations)

    # get LE data
    dfle = pd.DataFrame(columns=list(LE_LABELS.keys()) + ["Date", "Station"])
    for station in stations:
        # read the data and take daily mean
        dfletmp = get_data_from_le(station, variable="tpm10", labels="all")
        dfletmp = dfletmp.resample("D").mean()

        # set correct labels aggregation
        for label in LE_LABELS.keys():
            dfletmp[label] = dfletmp[LE_LABELS[label]].sum(axis=1)
            dfletmp.drop(LE_LABELS[label], axis=1, inplace=True)
        dfletmp["Station"] = station

        # add it to big dataframe
        dfle = pd.concat([dfle, dfletmp.reset_index()])

    # Merge both data
    dfle = dfle.reset_index().set_index(["Station", "Date"])
    dfOP = dfOP.reset_index().drop("index", axis=1).set_index(["Station", "Date"])
    df = dfle.merge(dfOP, right_index=True, left_index=True).drop("index", axis=1)

    return df


def set_LE_labels_names(df):
    """Convert and sum columns of labels to the appropriate names

    Defaults mapping between labels and names are defined in the ``env.py`` file, and are :

    .. code-block:: python

        LE_LABELS = {
            "Coal": [0, 3, 11, 14],
            "Biomass": [1, 4, 12, 15],
            "Other": [2, 5, 10, 13, 16, 21],
            "RoadTrans": [6, 17],
            "NonRoadTrans": [7, 18],
            "Industry": [8, 19],
            "Agriculture": [9, 20],
            "Natural": [22],
            "Boundary": [23],
        }

    Parameters
    ----------

    df : pd.DataFrame
        Dataframe with number as columns

    Returns
    -------

    dfrenamed : pd.DataFrame
    """
    dfrenamed = df.copy()
    for label in LE_LABELS.keys():
        dfrenamed[label] = dfrenamed[LE_LABELS[label]].sum(axis=1)
        dfrenamed.drop(LE_LABELS[label], inplace=True, axis=1)
    return dfrenamed


def _get_nc_file(date, excluded_vars=None):
    """Retrieve netcdf LE file for a given date

    Parameters
    ----------

    date : str, "%Y%m%d"
        Date to extract
    excluded_vars : list of str
        Drop this list of variables

    Returns
    -------

    ds : xarray dataset
    """
    day = date.date()
    filename = LE_NETCDF_FILENAME.format(year=date.year, date=date.strftime("%Y%m%d"))
    pathnc = DIR_LE_DATA + "/" + filename
    ds = xr.open_dataset(pathnc, drop_variables=excluded_vars)
    return ds


def get_data_from_le(station, variable=None, labels="sum_all"):
    """Extract data from netcdf

    If already extracted for this station coordinate, use it. Elsewhise, extract it from
    the big netcdf file.

    The dataframe returned has the specie renamed as follows:

        ========  ========
        variable  name
        ========  ========
        tmp10     PM10
        no3a_c    NO3-
        no3a_f    NO3-
        so4a_c    SO42-
        so4a_f    SO42-
        ec_f      EC
        ec_c      EC
        pom_c     POM
        pom_f     POM
        na        Na+
        tss       PM10_tss
        tdust     PM10_dust
        ppm       PM10_ppm
        ========  ========

    Parameters
    ----------
    station : str
        Name of the station
    variable : str or list of str, default: None
        tagged specie from LOTOS {tpm10, no3a_c, no3a_f, ...}
        If a list of str is provided, it read the individual file and sum them.
        I.e. ``[ec_f, ec_c]`` would result in the sum of ec_f + ec_c, or total EC.
    labels : "sum_all" or "all" or list of int, default: "sum_all"
        Choosen labels to extract. It could be:

          - sum_all : sum all labels
          - all : retreive all labels
          - list of int : retreive only theses labels

    Returns
    -------
    df : pd.DataFrame
        Concentration in µg/m³ for all given labels with datetime as index and labels as
        columns.
    """
    if variable is None:
        variable = "tpm10"

    new_var_mapper = {
        "tpm10": "PM10",
        "no3a": "NO3-",
        "so4a": "SO42-",
        "ec": "EC",
        "oc": "OC",
        "na": "Na+",
        "pom": "POM",
        "tss": "PM10_tss",
        "tdust": "PM10_tdust",
        "ppm": "PM10_ppm",
    }

    # ec_c, ec_f for instance
    if type(variable) == list:
        var = variable[0].split("_")[0]
        named_specie = new_var_mapper[var]
    else:
        named_specie = new_var_mapper[variable]

    # do not retrieve everything from netcdf
    excluded_vars = set(LE_VARIABLES) - set([named_specie])

    if type(variable) == list:
        # sum all fractions (c and f)
        dataLE = xr.open_dataset(
            DIR_LE_PER_STATION + "/{}_{}.nc".format(station, variable[0])
        )
        dataLE = dataLE.rename({variable[0]: named_specie})
        for var in variable[1:]:
            datatmp = xr.open_dataset(
                DIR_LE_PER_STATION + "/{}_{}.nc".format(station, var)
            )
            datatmp = datatmp.rename({var: named_specie})
            dataLE += datatmp
    else:
        dataLE = xr.open_dataset(
            DIR_LE_PER_STATION + "/{}_{}.nc".format(station, variable)
        )
        dataLE = dataLE.rename({variable: named_specie})

    # Select only wanted labels
    if labels == "sum_all":
        da = dataLE[named_specie].sum("label")  # .resample(time="D").mean()
    elif type(labels) == list:
        da = dataLE[named_specie].sel(label=labels)
    elif labels == "all":
        da = dataLE[named_specie]

    # Convert data aray to dataframe
    df = da.drop_vars(["longitude", "latitude", "level"]).to_dataframe()
    df = df.reset_index()
    if labels == "sum_all":
        df["Label"] = "all"
    df = df.rename(
        {"time": "Date", "label": "Label", named_specie: named_specie}, axis=1
    )
    df = df.pivot(index="Date", columns="Label", values=named_specie)
    if type(labels) == list:
        df.columns = labels

    # Convert to µg/m³
    df = df * 10 ** 9

    return df


def get_station_specie_data_PMF_LE(station, specie):
    """Get concentration data from PMF and LE for the station

    Parameters
    ----------

    station : str
    specie : str
        Mapping between PMF and LE :

        ======  ========   ================
        specie  PMF        LE
        ======  ========   ================
        PM10    PM10       tpm10
        SO42-   SO42-      so4a_c + so4a_f
        NO3-    NO3-       no3a_c + no3a_f
        EC      EC         ec_c + ec_f
        OC      OC         (pom_c + pom_f) / 1.4
        Na+     Na+        tss / 3.26
        ======  ========   ================


    Returns
    -------

    dfPMF : pd.DataFrame
    dfLE : pd.DataFrame
    """
    pmf_specie, le_specie = specie_names_to_LE_names(specie)

    pmf = get_pmf(station)
    dfPMF = pmf.to_cubic_meter(specie=pmf_specie)  # .sum(axis=1)
    dfPMF.index = dfPMF.index + pd.DateOffset(hours=12)

    dfLE = get_data_from_le(station=station, variable=le_specie, labels="all")

    return (dfPMF, dfLE)


def get_stations_contribution_from_pmf(stations, factors, specie, pmfs):
    """ "

    Parameters
    ----------

    stations : list of str
    factors : list of str
    specie : str
    pmfs : dict
        dict of PMF object with keys being in ``stations``

    Returns
    -------

    df : pd.DataFrame
    """

    df_dict = {}
    df = pd.DataFrame(columns=stations + ["Date"])
    for station in stations:
        if specie in pmfs[station].species:
            dfc = pmfs[station].to_cubic_meter(specie=specie)
        else:
            print(f"No {specie} for station {station}")
            continue

        if factors == "all":
            factors_ok = dfc.columns
        else:
            factors_ok = [factor for factor in factors if factor in dfc.columns]
            missing_factors = set(factors) - set(factors_ok)
            if missing_factors:
                for f in missing_factors:
                    print(f"No {f} for station {station}")

        dftmp = dfc[factors_ok].sum(axis=1, min_count=1) # min count to deal with NaN

        df_dict[station] = dftmp

    df = pd.concat(df_dict).to_frame()
    df.columns = ["+".join(factors_ok)]
    return df


def get_stations_contribution_from_le(stations, labels, specie):
    """Get contribution daily mean for the given station summed for the given labels

    Parameters
    ----------

    stations : list of str
    labels : list of str
        Key of the ``env.LE_LABELS`` dictionnary mapper
    variable : str

    Returns
    -------

    df : pd.DataFrame
    """

    df_dict = {}

    labels_int = []
    if labels == "all":
        labels_int = [i for x in LE_LABELS.values() for i in x]
    else:
        for label in labels:
            labels_int += LE_LABELS[label]

    _, le_specie = specie_names_to_LE_names(specie)

    for station in stations:
        dftmp = get_data_from_le(station=station, variable=le_specie, labels=labels_int)
        dftmp = dftmp.resample("D").mean().sum(axis=1)
        df_dict[station] = dftmp.to_frame()
    df = pd.concat(df_dict)
    return df


def plot_add_background(ax, text=True, addBackground=False, fontsize=12):
    """Add background to plot according to site typologies

    Parameters
    ----------

    ax : mpl.axe
        The axe to add the background on.
    text : boolean, True
    addBackground : boolean, True
    fontsize : int, 12
    """

    # nrural = len(TYPOLOGIES["Remote"])
    # nurban = len(TYPOLOGIES["Urban"])
    # nvalley = len(TYPOLOGIES["Urban valley"])
    # nindus = len(TYPOLOGIES["Industrial"])
    # ntraffic = len(TYPOLOGIES["Traffic"])

    ax.grid(False)
    xmin, xmax, ymin, ymax = ax.axis()
    textPos = ymax * (1 + ((ymax - ymin) / ymax) * 0.02)
    texts = TYPOLOGIES.keys()  # ["Rural", "Urban", "Urban+valley", "Traffic"]

    # restrict to current site only, not all possible ones
    actual_sites = [x.get_text() for x in ax.get_xticklabels()]
    nsites = [
        len([s for s in x["site"] if s in actual_sites]) for x in TYPOLOGIES.values()
    ]  # [nrural, nurban, nvalley, ntraffic]
    colors = [x["color"] for x in TYPOLOGIES.values()]
    basex, basey = (0, -0.12)

    xpos = xmin
    scale = 1 / 15
    for n, c, t in zip(nsites, colors, texts):
        if addBackground:
            rect = Rectangle((xpos, ymin), n, ymax - ymin, color=c, alpha=1, zorder=-1)
            ax.add_patch(rect)
        lxpos = xpos + 0.5 * n  # * scale
        if text:
            ax.text(
                lxpos, textPos, t, size=fontsize, ha="center"  # transform=ax.transAxes,
            )
        # add_line(ax, (xpos+0.5)*scale, basey)
        ax.vlines(x=xpos, ymin=ymin, ymax=ymax, linestyles="dashed")
        xpos += n
    ax.vlines(x=xpos, ymin=ymin, ymax=ymax, linestyles="dashed")
    ax.axis((xmin, xmax, ymin, ymax))
    # add_line(ax, (xpos+0.5)*scale, basey)


def save_station_to_disk(station, variables=None):
    """Extract from the LE output the values of each species for a given station.

    The LE netcdf should be in the ``DIR_LE_DATA`` folder, and the output are saved in the
    ``DIR_LE_PER_STATION`` (see env.py)

    It takes around 10 min per species for 2 years.

    Parameters
    ----------

    station : str
        Name of the station
    variables : list of str
        default to all possible variable, minus the gaseous part.

    """
    all_variables = [
        "no",
        "no2",
        "nh3",
        "so2",
        "hno3",
        "tpm10",
        "tpm25",
        "ec_f",
        "ec_c",
        "pom_f",
        "pom_c",
        "ppm_f",
        "ppm_c",
        "no3a_f",
        "no3a_c",
        "nh4a_f",
        "so4a_f",
        "so4a_c",
        "tpm25_comb",
        "tpm10_comb",
        "tss",
        "tdust",
    ]

    if variables is None:
        excluded_vars = ["no", "no2", "nh3", "so2", "hno3", "nh4f"]
        variables = list(set(all_variables) - set(excluded_vars))
    else:
        excluded_vars = list(set(all_variables) - set(variables))

    stations = get_station_coordinates([station])
    lon = stations["longitude"].values[0]
    lat = stations["latitude"].values[0]

    input_le_file = "{}/LE_PM_SoAp-FRA-FRA-201*".format(DIR_LE_DATA)
    datale = xr.open_mfdataset(
        input_le_file, parallel=True, drop_variables=excluded_vars
    )

    datale = (
        datale.sel(longitude=lon, latitude=lat, method="nearest")
        .sel(level=0)
        .drop_dims(["longitude_crnr", "latitude_crnr"])
    )

    print(variables)
    for variable in variables:
        print(dt.now().strftime("%d %H:%M"), station, variable)
        ds = datale[variable]
        output_file = "{}/{}_{}.nc".format(DIR_LE_PER_STATION, station, variable)
        ds.to_netcdf(output_file)

def add_new_variable_as_nc_file(station, variable=None):
    """Create new nc file for the given variables
    
    All generated netcdf file have the name {station}_{specie}{_fraction}.nc,
    but some specie are "hide" into some other. For instance, ``Na+ is tss / 3.26`. This
    is messing when trying to retrieve bulk data... so for create these nc file, then
    retrieve them.


        ======== ====================
        variable computation
        ======== ====================
        Na+      tss / 3.26
        OC       (pom_c + pom_f)/1.4
        ======== ====================

    Parameters
    ----------

    station : str
    variable : str, {"Na+", "OC"}
    """

    if variable == "Na+":
        tss = xr.open_dataset(
            DIR_LE_PER_STATION + "/{}_{}.nc".format(station, 'tss')
        )
        new = tss / 3.26
        new = new.rename({"tss": "na"})
        filename = f"{station}_na.nc"
    elif variable == "OC":
        pom_f = xr.open_dataset(
            DIR_LE_PER_STATION + "/{}_{}.nc".format(station, 'pom_f')
        )
        pom_c = xr.open_dataset(
            DIR_LE_PER_STATION + "/{}_{}.nc".format(station, 'pom_c')
        )
        pom_f = pom_f.rename({"pom_f": "oc"})
        pom_c = pom_c.rename({"pom_c": "oc"})
        new = (pom_f + pom_c) / 1.4
        filename = f"{station}_oc.nc"
    else:
        ValueError(
        """variable should be in {Na+, OC}, but {} was provided""".format(variable))

    new.to_netcdf(DIR_LE_PER_STATION + "/" + filename)

def save_all_station_to_disk(stations, variables=None):
    """Extract all values from the netcdf LE output for the given stations.

    The LE netcdf should be in the ``DIR_LE_DATA`` folder, and the output are saved in the
    ``DIR_LE_PER_STATION`` (see env.py)

    Parameters
    ----------

    stations : list of str
        Names of the stations
    variables : list of str
        Variable to extracte from LE.
    """
    if variables is None:
        variables = ["tpm10"]

    for station in stations:
        save_station_to_disk(station, variables)
        add_new_variable_as_nc_file(station, "Na+")
        add_new_variable_as_nc_file(station, "OC")
