
Documentation of the API : [https://webersa.gricad-pages.univ-grenoble-alpes.fr/comparison-le-pmf](https://webersa.gricad-pages.univ-grenoble-alpes.fr/comparison-le-pmf)

# Comparison of LOTOS-EUROS model output and PMF

Specie contribution and source apportionment comparison between deterministic model
(Lotos-Euros) and observation-base model (PMF).

Each PMF has been run separatly, and a details discussion is available at
http://pm-sources.u-ga.fr .

Lotos-Euros (LE) was run with tagging (species, sources and geographical origin) from
2013-01 to 2016-12 to cover most of the PMF studies available.


# How to

Make a virtual environment and install dependancies :


     python3 -m venv venv
     source venv/bin/activate
     pip install -r requirements.txt



