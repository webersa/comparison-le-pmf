Comparison of LOTOS-EUROS and PMF source-apportionment
======================================================

Specie contribution and source apportionment comparison between deterministic model
(Lotos-Euros) and observation-base model (PMF).


PMF outputs
-----------

Each PMF has been run separatly, and a details discussion is available at
http://pm-sources.u-ga.fr .


LOTOS-EUROS outputs
-------------------

Lotos-Euros (LE) was run with tagging (species, sources and geographical origin) from
2013-01 to 2016-12 to cover most of the PMF studies available.


.. toctree::

   API
   Article
